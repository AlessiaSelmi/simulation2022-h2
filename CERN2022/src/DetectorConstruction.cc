//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the B1::DetectorConstruction class

#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4RegionStore.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"


// Color
#include "G4VisAttributes.hh"
#include "G4Colour.hh"





//namespace B1
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
DetectorConstruction::~DetectorConstruction()
{}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // materiali
    // Get nist material manager
    G4NistManager* nist = G4NistManager::Instance();
    // camere
    G4Material* Silicon = nist->FindOrBuildMaterial("G4_Si");
    // cryst
    G4Material* PbWO= nist->FindOrBuildMaterial("G4_PbWO4");    
   
    // lead glass
    G4Material* PbO = nist->FindOrBuildMaterial("G4_GLASS_LEAD");

    
    // Altro modo per fare il lead glass
//     // This function illustrates the possible ways to define materials using
//     // G4 database on G4Elements
//     G4Material* PbO = new G4Material("PbO", density=  9.530 *g/cm3, ncomponents=2);
//     G4Element* O  = nist->FindOrBuildElement(8);
//     PbO->AddElement(O , natoms=1);
//     G4Element* Pb = nist->FindOrBuildElement(82);
//     PbO->AddElement(Pb, natoms= 1);

    G4bool checkOverlaps = true;
    // world material
    G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
		
		
    // volume logico + placement del mondo
    //-- WORLD
    G4Box* solidWorld = new G4Box("World",10*m,10*m,50*m);
    G4LogicalVolume* logicWorld = new G4LogicalVolume(solidWorld, world_mat, "World");
    G4VPhysicalVolume* physWorld = new G4PVPlacement
                        (0,                    // no rotation
                        G4ThreeVector(),       // centre position
                        logicWorld,            // its logical volume
                        "World",               // its name
                        0,                     // its mother volume
                        false,                 // no boolean operation
                        0,                     // copy number
                        checkOverlaps);        // overlaps checking
						
						
	// Il mondo deve essere nascosto, sennò non vedo dentro
	G4VisAttributes* worldColor= new G4VisAttributes();
	worldColor->SetVisibility(false);
	logicWorld->SetVisAttributes(worldColor);
	

// volumi logici + placemnet 
//-----------------------------------------------------------------------------------------------------------------
    // Telescope
	G4Box* Tele = new G4Box("Tele",2*cm/2, 2*cm/2, 410*um/2);
    G4LogicalVolume* fLogicTele = new G4LogicalVolume(Tele, Silicon,"Tele");
    new G4PVPlacement(0, 
					G4ThreeVector(0, 0, 0.*m), 
					fLogicTele, 
					"Tele1", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);


	
//-----------------------------------------------------------------------------------------------------------------
    // Telescope 2
    new G4PVPlacement(0, 
					G4ThreeVector(0, 0, 15.9*m), 
					fLogicTele, 
					"Tele2", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);



//-----------------------------------------------------------------------------------------------------------------




//-----------------------------------------------------------------------------------------------------------------
    // Magnet
    G4Box* Magnet = new G4Box("Magnet",40*cm/2, 40*cm/2, 2*m/2); // TODO: Check other dimension!!
    G4LogicalVolume* logicMagnet = new G4LogicalVolume(Magnet,world_mat,"Magnet");
    new G4PVPlacement(0,
					G4ThreeVector(0, 0, 0.67*m + 5.11*m + (3/2)*m),
					logicMagnet,
					"Magnet",
					logicWorld,
					false,
					0,
					checkOverlaps);




//-----------------------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------
    // PbWO Crystal1
	//Spessore in unità di X0
	G4double thicknessCrist = 4.5 * PbWO->GetRadlen(); 

	
    G4Box* Crystal = new G4Box("Crystal", 3*cm/2, 3*cm/2, thicknessCrist/2); 
    //G4LogicalVolume* logicCrystal = new G4LogicalVolume(Crystal,PbWO,"Crystal");
    //se vuoi run di calibrazione (no cristallo su fascio)
    G4LogicalVolume* logicCrystal = new G4LogicalVolume(Crystal,world_mat,"Crystal");  
    
    G4double lcryst = 3*cm;
    new G4PVPlacement(0,
					G4ThreeVector(0, 0, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalCC",
					logicWorld,
					false,
					0,
					checkOverlaps);

    new G4PVPlacement(0,
					G4ThreeVector(lcryst,0, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalCR",
					logicWorld,
					false,
					0,
					checkOverlaps);

    new G4PVPlacement(0,
					G4ThreeVector(0, lcryst, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalTC",
					logicWorld,
					false,
					0,
					checkOverlaps);
    
    new G4PVPlacement(0,
					G4ThreeVector(lcryst, lcryst, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalTR",
					logicWorld,
					false,
					0,
					checkOverlaps);
    
    new G4PVPlacement(0,
					G4ThreeVector(-lcryst, 0, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalCL",
					logicWorld,
					false,
					0,
					checkOverlaps);

    new G4PVPlacement(0,
					G4ThreeVector(-lcryst, lcryst, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalTL",
					logicWorld,
					false,
					0,
					checkOverlaps);
    
    new G4PVPlacement(0,
					G4ThreeVector(-lcryst, -lcryst, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalBL",
					logicWorld,
					false,
					0,
					checkOverlaps);
    
    new G4PVPlacement(0,
					G4ThreeVector(lcryst, -lcryst, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalBR",
					logicWorld,
					false,
					0,
					checkOverlaps);
    new G4PVPlacement(0,
					G4ThreeVector(0, -lcryst, 15.90*m+1.57*m),
					logicCrystal,
					"CrystalBC",
					logicWorld,
					false,
					0,
					checkOverlaps);


//-----------------------------------------------------------------------------------------------------------------



//=================================================================================================================
// Begin of Calorimeter
	
	G4double distCalo = 15.90*m + 1.57*m + 0.29*m;
    G4double lcalo = 10*cm;
// 	// Centro di genni
// 	G4double x0_genni = 0*cm;
// 	G4double z0_genni = distCalo + 32*cm/2;

//-----------------------------------------------------------------------------------------------------------------
    // LGs
    // centrale
    
    
    
    G4Box* LG = new G4Box("LG", lcalo/2, lcalo/2, 40*cm/2);
    G4LogicalVolume* fLogicLG = new G4LogicalVolume(LG, PbO,"LG");
    new G4PVPlacement(0, 
					G4ThreeVector(0, 0, distCalo), 
					fLogicLG, 
					"LGcc", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    // dx e sx rispetto a centrale
    new G4PVPlacement(0, 
					G4ThreeVector(-lcalo, 0, distCalo), 
					fLogicLG, 
					"LGcl", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    new G4PVPlacement(0, 
					G4ThreeVector(lcalo, 0, distCalo), 
					fLogicLG, 
					"LGcr", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 

// mettiamo un po' a caso i LG sopra
    new G4PVPlacement(0, 
					G4ThreeVector(lcalo/2, lcalo, distCalo), 
					fLogicLG, 
					"LGtr", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    new G4PVPlacement(0, 
					G4ThreeVector(-lcalo/2, lcalo, distCalo), 
					fLogicLG, 
					"LGtl", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    // mettiamo un po' a caso i LG sotto
    new G4PVPlacement(0, 
					G4ThreeVector(lcalo/2, -lcalo, distCalo), 
					fLogicLG, 
					"LGbr", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    new G4PVPlacement(0, 
					G4ThreeVector(-lcalo/2, -lcalo, distCalo), 
					fLogicLG, 
					"LGbl", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);
    // inseriamo lead glass a lato per vedere se perdo energia 
    new G4PVPlacement(0, 
					G4ThreeVector(-2*lcalo, 0, distCalo), 
					fLogicLG, 
					"LGcls", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);
    
    
    new G4PVPlacement(0, 
					G4ThreeVector(2*lcalo, 0, distCalo), 
					fLogicLG, 
					"LGcrs", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    
    new G4PVPlacement(0, 
					G4ThreeVector(-3/2*lcalo, lcalo, distCalo), 
					fLogicLG, 
					"LGtls", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    new G4PVPlacement(0, 
					G4ThreeVector(+3/2*lcalo, lcalo, distCalo), 
					fLogicLG, 
					"LGtls", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    new G4PVPlacement(0, 
					G4ThreeVector(-3*lcalo/2, -lcalo, distCalo), 
					fLogicLG, 
					"LGbls", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps);
    new G4PVPlacement(0, 
					G4ThreeVector(3*lcalo/2, -lcalo, distCalo), 
					fLogicLG, 
					"LGbrs", 
					logicWorld, 
					false, 
					0, 
					checkOverlaps); 
    
// END OF SETUP
//=================================================================================================================
//					G4ThreeVector(x0_LG - 4*11*cm*cos(CLHEP::pi/2*rad - Angle), 0, z0_LG + 4*11*cm*sin(CLHEP::pi/2*rad - Angle)), 

	




	
	
	
	
	
	
	
	
	// Colori dei vari pezzi



G4Color red(1.0, 0.0, 0.0);
G4Color blue(0.0, 0.0, 1.0);
G4Colour green(0.0, 1.0, 0.0);
G4Colour magenta(1.0, 0.0, 1.0);

	// Silici = Rosso
	G4VisAttributes* siliColor = new G4VisAttributes(red);
	siliColor->SetVisibility(true);
	siliColor->SetForceSolid(true);
	
	fLogicTele->SetVisAttributes(siliColor);
	

	
	
	// Magnet = yellow
	G4VisAttributes* magColor = new G4VisAttributes(magenta);
	magColor->SetVisibility(true);
	magColor->SetForceSolid(true);
	
	logicMagnet->SetVisAttributes(magColor);
    


	// Crystal = blu
	G4VisAttributes* crystalColor = new G4VisAttributes(blue);
	crystalColor->SetVisibility(true);
	crystalColor->SetForceSolid(true);
	
	logicCrystal->SetVisAttributes(crystalColor);
		
		
		
		
		
	// Calo A = Verdi
	G4VisAttributes* caloA = new G4VisAttributes(green);
	caloA->SetVisibility(true);
	caloA->SetForceSolid(true);
	

	fLogicLG->SetVisAttributes(caloA);



  //always return the physical World
  return physWorld;
}

void DetectorConstruction::ConstructSDandField()
{
    // =============================
    //       MAGNETIC FIELD
    // =============================
    G4double fieldValue = 0*tesla*m / (2*m);
    G4UniformMagField* myField = new G4UniformMagField(G4ThreeVector(0., fieldValue, 0.));
    G4FieldManager* fieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
    G4LogicalVolume* logicBox1 = G4LogicalVolumeStore::GetInstance()->GetVolume("Magnet");
    G4FieldManager* localfieldMgr = new G4FieldManager(myField);
    logicBox1->SetFieldManager(localfieldMgr,true);
    fieldMgr->CreateChordFinder(myField);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}
