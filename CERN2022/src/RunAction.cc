//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file RunAction.cc
/// \brief Implementation of the B1::RunAction class

#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"
#include "G4AnalysisManager.hh"

#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//namespace B1
namespace { G4Mutex stuffMutex = G4MUTEX_INITIALIZER; }
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction()
    : G4UserRunAction()
{
    //using analysis manager for output
    auto analysisManager = G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run* run)
{
    G4RunManager::GetRunManager()->SetPrintProgress(run->GetNumberOfEventToBeProcessed()/100);

    // inform the runManager to save random number seed
    G4RunManager::GetRunManager()->SetRandomNumberStore(false);

    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------Begin of Global Run-----------------------";
    }
    else {
        G4int ithread = G4Threading::G4GetThreadId();

        G4cout
         << G4endl
         << "--------------------Begin of Local Run------------------------"
         << G4endl;
    }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run* run)
{

    G4int nofEvents = run->GetNumberOfEvent();

    // Run conditions
    //  note: There is no primary generator action object for "master"
    //        run manager for multi-threaded mode.

    // Print
    //
    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------End of Global Run-----------------------";
    }
    else {

      G4cout
       << G4endl
       << "--------------------End of Local Run------------------------";



      static bool first = true;
      stuffMutex.lock();

      std::ofstream energydepH2; // nome del file finale da salvare 

      if (first)
      {

		  energydepH2.open("energydepH2.dat");
          first = false;
		  
		  energydepH2<<"crystalCC\tcrystalCL\tcrystalCR\tcrystalTC\tcrystalTL\tcrystalTR\tcrystalBC\tcrystalBL\tcrystalBR\tLGbl\tLGbr\tLGcl\tLGcc\tLGcr\tLGtl\tLGtr\tLGbls\tLGbrs\tLGcls\tLGcrs\tLGtls\tLGtrs"<<std::endl;

      }
      else
      {
		  energydepH2.open("energydepH2.dat", std::ios_base::app);
      }
	  
	  
    
      for (int i = 0; i < E_crystalCC.size(); i++) 
      {
          energydepH2<<E_crystalCC[i]<<"\t"<<E_crystalCL[i]<<"\t"<<E_crystalCR[i]<<"\t"<<E_crystalTC[i]<<"\t"<<E_crystalTL[i]<<"\t"<<E_crystalTR[i]<<"\t"<<E_crystalBC[i]<<"\t"<<E_crystalBL[i]<<"\t"<<E_crystalBR[i]<<"\t"<<E_LGbl[i]<<"\t"<<E_LGbr[i]<<"\t"<<E_LGcl[i]<<"\t"<<E_LGcc[i]<<"\t"<<E_LGcr[i]<<"\t"<<E_LGtl[i]<<"\t"<<E_LGtr[i]<<"\t"<<E_LGbls[i]<<"\t"<<E_LGbrs[i]<<"\t"<<E_LGcls[i]<<"\t"<<E_LGcrs[i]<<"\t"<<E_LGtls[i]<<"\t"<<E_LGtrs[i]<<"\t"<<std::endl;

      }
	  
	  
	  
	  
	  
	  energydepH2.close();

      stuffMutex.unlock();

    }

//    static G4Mutex stuffMutex = G4MUTEX_INITIALIZER;

    //stuffMutex.lock();

 // stuffMutex.unlock();

    G4cout
       << G4endl
       << " The run consists of " << nofEvents << " particles"
       << G4endl
       << "------------------------------------------------------------"
       << G4endl
       << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}
