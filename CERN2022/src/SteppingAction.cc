//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SteppingAction.cc
/// \brief Implementation of the B1::SteppingAction class

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"

#include "G4AnalysisManager.hh"

//namespace B1
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* eventAction)
: fEventAction(eventAction)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{


    G4VPhysicalVolume* volume
        = step->GetPreStepPoint()->GetPhysicalVolume();
          
          
		  


  
	
	
	// PMG Sim
	if(volume->GetName()=="CrystalCC")
    {
        fEventAction->AddDepositedEnergyCrystalCC(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="CrystalCL")
    {
        fEventAction->AddDepositedEnergyCrystalCL(step->GetTotalEnergyDeposit()/GeV);
    }
    
	if(volume->GetName()=="CrystalCR")
    {
        fEventAction->AddDepositedEnergyCrystalCR(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="CrystalTC")
    {
        fEventAction->AddDepositedEnergyCrystalTC(step->GetTotalEnergyDeposit()/GeV);
    }
    
    if(volume->GetName()=="CrystalTL")
    {
        fEventAction->AddDepositedEnergyCrystalTL(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="CrystalTR")
    {
        fEventAction->AddDepositedEnergyCrystalTR(step->GetTotalEnergyDeposit()/GeV);
    }
    
    if(volume->GetName()=="CrystalBC")
    {
        fEventAction->AddDepositedEnergyCrystalBC(step->GetTotalEnergyDeposit()/GeV);
    }
	
    if(volume->GetName()=="CrystalBL")
    {
        fEventAction->AddDepositedEnergyCrystalBL(step->GetTotalEnergyDeposit()/GeV);
    }
    
    if(volume->GetName()=="CrystalBR")
    {
        fEventAction->AddDepositedEnergyCrystalBR(step->GetTotalEnergyDeposit()/GeV);
    }
	


	if(volume->GetName()=="LGbl")
    {
        fEventAction->AddDepositedEnergyLGbl(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGbr")
    {
        fEventAction->AddDepositedEnergyLGbr(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGcl")
    {
        fEventAction->AddDepositedEnergyLGcl(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGcc")
    {
        fEventAction->AddDepositedEnergyLGcc(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGcr")
    {
        fEventAction->AddDepositedEnergyLGcr(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGtl")
    {
        fEventAction->AddDepositedEnergyLGtl(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGtr")
    {
        fEventAction->AddDepositedEnergyLGtr(step->GetTotalEnergyDeposit()/GeV);
    }
    
    if(volume->GetName()=="LGbls")
    {
        fEventAction->AddDepositedEnergyLGbls(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGbrs")
    {
        fEventAction->AddDepositedEnergyLGbrs(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGcls")
    {
        fEventAction->AddDepositedEnergyLGcls(step->GetTotalEnergyDeposit()/GeV);
    }
    
	if(volume->GetName()=="LGcrs")
    {
        fEventAction->AddDepositedEnergyLGcrs(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGtls")
    {
        fEventAction->AddDepositedEnergyLGtls(step->GetTotalEnergyDeposit()/GeV);
    }
	if(volume->GetName()=="LGtrs")
    {
        fEventAction->AddDepositedEnergyLGtrs(step->GetTotalEnergyDeposit()/GeV);
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}

