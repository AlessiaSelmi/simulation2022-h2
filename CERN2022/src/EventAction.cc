//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EventAction.cc
/// \brief Implementation of the B1::EventAction class

#include "EventAction.hh"
#include "RunAction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

//namespace B1
//{

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction(RunAction* runAction)
: fRunAction(runAction)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::BeginOfEventAction(const G4Event* event)
{
	
	
	// PMG Sim // all'inizio di ogni evento vuoi energia a zero (obviously) 
	E_crystalCC = 0;
	E_crystalCL = 0;
    E_crystalCR = 0;
	E_crystalTL = 0;
    E_crystalTC = 0;
	E_crystalTR = 0;
    E_crystalBC = 0;
	E_crystalBL = 0;
    E_crystalBR = 0;

	E_LGbl = 0;
	E_LGbr = 0;
	E_LGcl = 0;
	E_LGcc = 0;
	E_LGcr = 0;
	E_LGtl = 0;
	E_LGtr = 0;
    
    E_LGbls = 0;
	E_LGbrs = 0;
	E_LGcls = 0;
	E_LGcrs = 0;
	E_LGtls = 0;
	E_LGtrs = 0;
	
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::EndOfEventAction(const G4Event*)
{

	
	
	// funzione definita in RunAction.hh che appende al vettore le energie di ogni evento
    fRunAction->AddDepositedEnergyCrystalCC(E_crystalCC);
    fRunAction->AddDepositedEnergyCrystalCR(E_crystalCR);
    fRunAction->AddDepositedEnergyCrystalCL(E_crystalCL);
    fRunAction->AddDepositedEnergyCrystalTC(E_crystalTC);
    fRunAction->AddDepositedEnergyCrystalTR(E_crystalTR);
    fRunAction->AddDepositedEnergyCrystalTL(E_crystalTL);
    fRunAction->AddDepositedEnergyCrystalBC(E_crystalBC);
    fRunAction->AddDepositedEnergyCrystalBR(E_crystalBR);
    fRunAction->AddDepositedEnergyCrystalBL(E_crystalBL);

    fRunAction->AddDepositedEnergyLGbl(E_LGbl);
    fRunAction->AddDepositedEnergyLGbr(E_LGbr);
    fRunAction->AddDepositedEnergyLGcl(E_LGcl);
    fRunAction->AddDepositedEnergyLGcc(E_LGcc);
    fRunAction->AddDepositedEnergyLGcr(E_LGcr);
    fRunAction->AddDepositedEnergyLGtl(E_LGtl);
    fRunAction->AddDepositedEnergyLGtr(E_LGtr);
    
    
    fRunAction->AddDepositedEnergyLGbls(E_LGbls);
    fRunAction->AddDepositedEnergyLGbrs(E_LGbrs);
    fRunAction->AddDepositedEnergyLGcls(E_LGcls);
    fRunAction->AddDepositedEnergyLGcrs(E_LGcrs);
    fRunAction->AddDepositedEnergyLGtls(E_LGtls);
    fRunAction->AddDepositedEnergyLGtrs(E_LGtrs);

}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}
