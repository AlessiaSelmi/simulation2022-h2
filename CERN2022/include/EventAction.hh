//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EventAction.hh
/// \brief Definition of the B1::EventAction class

#ifndef B1EventAction_h
#define B1EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

/// Event action class
///

//namespace B1
//{

class RunAction;

class EventAction : public G4UserEventAction
{
  public:
    EventAction(RunAction* runAction);
    ~EventAction() override;

    void BeginOfEventAction(const G4Event* event) override;
    void EndOfEventAction(const G4Event* event) override;
	
	
	// PMG Sim
    void AddDepositedEnergyCrystalCC(G4double Edep) {E_crystalCC+=Edep;}
    void AddDepositedEnergyCrystalCL(G4double Edep) {E_crystalCL+=Edep;}
    void AddDepositedEnergyCrystalCR(G4double Edep) {E_crystalCR+=Edep;}
    void AddDepositedEnergyCrystalTC(G4double Edep) {E_crystalTC+=Edep;}
    void AddDepositedEnergyCrystalTR(G4double Edep) {E_crystalTR+=Edep;}
    void AddDepositedEnergyCrystalTL(G4double Edep) {E_crystalTL+=Edep;}
    void AddDepositedEnergyCrystalBC(G4double Edep) {E_crystalBC+=Edep;}
    void AddDepositedEnergyCrystalBL(G4double Edep) {E_crystalBL+=Edep;}
    void AddDepositedEnergyCrystalBR(G4double Edep) {E_crystalBR+=Edep;}

    void AddDepositedEnergyLGbl(G4double Edep) {E_LGbl += Edep;}
    void AddDepositedEnergyLGbr(G4double Edep) {E_LGbr += Edep;}
    void AddDepositedEnergyLGcl(G4double Edep) {E_LGcl += Edep;}
    void AddDepositedEnergyLGcc(G4double Edep) {E_LGcc += Edep;}
    void AddDepositedEnergyLGcr(G4double Edep) {E_LGcr += Edep;}
    void AddDepositedEnergyLGtl(G4double Edep) {E_LGtl += Edep;}
    void AddDepositedEnergyLGtr(G4double Edep) {E_LGtr += Edep;}

    
    void AddDepositedEnergyLGbls(G4double Edep) {E_LGbls += Edep;}
    void AddDepositedEnergyLGbrs(G4double Edep) {E_LGbrs += Edep;}
    void AddDepositedEnergyLGcls(G4double Edep) {E_LGcls += Edep;}
    void AddDepositedEnergyLGcrs(G4double Edep) {E_LGcrs += Edep;}
    void AddDepositedEnergyLGtls(G4double Edep) {E_LGtls += Edep;}
    void AddDepositedEnergyLGtrs(G4double Edep) {E_LGtrs += Edep;}


  private:
    RunAction* fRunAction;
	
	
	// PMG Sim
	G4double E_crystalCC;
	G4double E_crystalCL;
    G4double E_crystalCR;
	G4double E_crystalTC;
    G4double E_crystalTR;
	G4double E_crystalTL;
    G4double E_crystalBC;
	G4double E_crystalBL;
    G4double E_crystalBR;
	
	// Note! Not in this order
	G4double E_LGbl;
	G4double E_LGbr;
	G4double E_LGcl;
	G4double E_LGcc;
	G4double E_LGcr;
	G4double E_LGtl;
	G4double E_LGtr;
	
    G4double E_LGbls;
	G4double E_LGbrs;
	G4double E_LGcls;
	G4double E_LGcrs;
	G4double E_LGtls;
	G4double E_LGtrs;
	
	
	
	
};

//}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


