//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file RunAction.hh
/// \brief Definition of the B1::RunAction class

#ifndef B1RunAction_h
#define B1RunAction_h 1

#include "G4UserRunAction.hh"
#include "G4Accumulable.hh"
#include "G4AutoLock.hh"
#include <vector>
#include "globals.hh"
#include <fstream>

class G4Run;

/// Run action class
///
/// In EndOfRunAction(), it calculates the dose in the selected volume
/// from the energy deposit accumulated via stepping and event actions.
/// The computed dose is then printed on the screen.

//namespace B1
//{

class RunAction : public G4UserRunAction
{
  public:
    RunAction();
    ~RunAction() override;

    void BeginOfRunAction(const G4Run*) override;
    void   EndOfRunAction(const G4Run*) override;

	
	// PMG Sim
	void AddDepositedEnergyCrystalCC(G4double Edep) {E_crystalCC.push_back(Edep);}
	void AddDepositedEnergyCrystalCL(G4double Edep) {E_crystalCL.push_back(Edep);}
    void AddDepositedEnergyCrystalCR(G4double Edep) {E_crystalCR.push_back(Edep);}
	void AddDepositedEnergyCrystalTC(G4double Edep) {E_crystalTC.push_back(Edep);}
    void AddDepositedEnergyCrystalTL(G4double Edep) {E_crystalTL.push_back(Edep);}
	void AddDepositedEnergyCrystalTR(G4double Edep) {E_crystalTR.push_back(Edep);}
    void AddDepositedEnergyCrystalBC(G4double Edep) {E_crystalBC.push_back(Edep);}
	void AddDepositedEnergyCrystalBL(G4double Edep) {E_crystalBL.push_back(Edep);}
    void AddDepositedEnergyCrystalBR(G4double Edep) {E_crystalBR.push_back(Edep);}


	void AddDepositedEnergyLGbl(G4double Edep) {E_LGbl.push_back(Edep);}
	void AddDepositedEnergyLGbr(G4double Edep) {E_LGbr.push_back(Edep);}
	void AddDepositedEnergyLGcl(G4double Edep) {E_LGcl.push_back(Edep);}
	void AddDepositedEnergyLGcc(G4double Edep) {E_LGcc.push_back(Edep);}
	void AddDepositedEnergyLGcr(G4double Edep) {E_LGcr.push_back(Edep);}
	void AddDepositedEnergyLGtl(G4double Edep) {E_LGtl.push_back(Edep);}
	void AddDepositedEnergyLGtr(G4double Edep) {E_LGtr.push_back(Edep);}
	
	
    void AddDepositedEnergyLGbls(G4double Edep) {E_LGbls.push_back(Edep);}
	void AddDepositedEnergyLGbrs(G4double Edep) {E_LGbrs.push_back(Edep);}
	void AddDepositedEnergyLGcls(G4double Edep) {E_LGcls.push_back(Edep);}
	void AddDepositedEnergyLGcrs(G4double Edep) {E_LGcrs.push_back(Edep);}
	void AddDepositedEnergyLGtls(G4double Edep) {E_LGtls.push_back(Edep);}
	void AddDepositedEnergyLGtrs(G4double Edep) {E_LGtrs.push_back(Edep);}




  private:

	
	// PMG Sim
	std::vector<G4double> E_crystalCC;
	std::vector<G4double> E_crystalCL;
    std::vector<G4double> E_crystalCR;
	std::vector<G4double> E_crystalTC;
    std::vector<G4double> E_crystalTL;
	std::vector<G4double> E_crystalTR;
    std::vector<G4double> E_crystalBC;
	std::vector<G4double> E_crystalBL;
    std::vector<G4double> E_crystalBR;

	
	// Note! Not in this order
	std::vector<G4double> E_LGbl;
	std::vector<G4double> E_LGbr;
	std::vector<G4double> E_LGcl;
	std::vector<G4double> E_LGcc;
	std::vector<G4double> E_LGcr;
	std::vector<G4double> E_LGtl;
	std::vector<G4double> E_LGtr;
    
    std::vector<G4double> E_LGbls;
	std::vector<G4double> E_LGbrs;
	std::vector<G4double> E_LGcls;
	std::vector<G4double> E_LGcrs;
	std::vector<G4double> E_LGtls;
	std::vector<G4double> E_LGtrs;

	
};

//}

#endif

